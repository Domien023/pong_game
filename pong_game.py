import pygame as pg
from sys import exit
from random import choice, randint

class Platform(pg.sprite.Sprite):
    def __init__(self, player):
        super().__init__()
        self.image = pg.Surface((9, 80))
        if player == "p1":
            self.rect = self.image.get_rect(midtop=(780, 200))
        else:
            self.rect = self.image.get_rect(midtop=(20, 200))
        self.image.fill("white")

    def platform_movement(self):
        keys = pg.key.get_pressed()
        if self.rect.x >= 750:
            if keys[pg.K_UP] and self.rect.top >= 0:
                self.rect.y -= 5
            if keys[pg.K_DOWN] and self.rect.bottom <= 500:
                self.rect.y += 5
        else:
            if keys[pg.K_w] and self.rect.top >= 0:
                self.rect.y -= 5
            if keys[pg.K_s] and self.rect.bottom <= 500:
                self.rect.y += 5

    def update(self):
        self.platform_movement()


class Ball(pg.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.text_font = pg.font.Font("font/Pixeltype.ttf", 250)
        self.image = pg.image.load("graphics/ball.png").convert_alpha()
        self.rect = self.image.get_rect(center=(400, 250))
        self.speed_x_list = [-5, 5]
        self.speed_y_list = [-3, 3]
        self.speed_x = choice(self.speed_x_list)
        self.speed_y = choice(self.speed_y_list)
        self.p1_score = 0
        self.p2_score = 0
        self.bounce_sound = pg.mixer.Sound("audio/bounce.mp3")

    def score(self):
        if self.rect.right <= 0:
            self.p2_score += 1
            self.rect.center = (400, randint(100, 400))
            self.speed_x = choice(self.speed_x_list)
            self.speed_y = choice(self.speed_y_list)
        elif self.rect.left >= 800:
            self.p1_score += 1
            self.rect.center = (400, randint(100, 400))
            self.speed_x = choice(self.speed_x_list)
            self.speed_y = choice(self.speed_y_list)

    def display_score(self, surf):
        p1_score_message = self.text_font.render(f"{self.p1_score}", False, "white")
        p1_score_message_rect = p1_score_message.get_rect(center=(300, 90))
        p2_score_message = self.text_font.render(f"{self.p2_score}", False, "white")
        p2_score_message_rect = p2_score_message.get_rect(center=(500, 90))
        surf.blit(p1_score_message, p1_score_message_rect)
        surf.blit(p2_score_message, p2_score_message_rect)

    def ball_movement(self):
        self.rect.x += self.speed_x
        if self.rect.top <= 0 or self.rect.bottom >= 500:
            self.speed_y *= (-1)
        self.rect.y += self.speed_y

    def collisions(self):
        if pg.sprite.spritecollide(ball.sprite, platform, False):
            self.speed_x *= (-1)
            self.bounce_sound.play()

    def change_speed(self):
        self.speed_x_list = pong.change_level()

    def update(self):
        self.ball_movement()
        self.collisions()
        self.score()
        self.display_score(pong.screen)
        self.change_speed()


class Game:
    def __init__(self):
        pg.init()
        self.screen = pg.display.set_mode((800, 500))
        pg.display.set_caption("Pong")
        self.clock = pg.time.Clock()
        self.text_font = pg.font.Font("font/Pixeltype.ttf", 250)
        self.button_text = pg.font.Font("font/Pixeltype.ttf", 50)
        self.run = False
        self.box = pg.Rect(0, 0, 150, 60)
        self.button_start_color = "black"
        self.button_easy_color = "white"
        self.button_medium_color = "black"
        self.button_hard_color = "black"
        self.button_start = self.button_text.render("START", False, "white")
        self.button_start_rect = self.button_start.get_rect(center=(400, 350))
        self.button_easy = self.button_text.render("EASY", False, "white")
        self.button_easy_rect = self.button_easy.get_rect(center=(150, 200))
        self.button_medium = self.button_text.render("MEDIUM", False, "white")
        self.button_medium_rect = self.button_medium.get_rect(center=(400, 200))
        self.button_hard = self.button_text.render("HARD", False, "white")
        self.button_hard_rect = self.button_hard.get_rect(center=(650, 200))

    def main(self):
        while True:
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    pg.quit()
                    exit()
                if event.type == pg.MOUSEBUTTONDOWN:
                    if self.button_start_rect.collidepoint(event.pos):
                        self.button_start_color = self.button_click_start()

                    elif self.button_easy_rect.collidepoint(event.pos):
                        self.button_easy_color = self.button_click_level()

                    elif self.button_medium_rect.collidepoint(event.pos):
                        self.button_medium_color = self.button_click_level()

                    elif self.button_hard_rect.collidepoint(event.pos):
                        self.button_hard_color = self.button_click_level()

                if event.type == pg.MOUSEBUTTONUP:
                    if self.button_start_rect.collidepoint(event.pos):
                        self.run = True

            if self.run:
                self.screen.fill("black")
                platform.draw(self.screen)
                platform.update()

                for line in range(17):
                    pg.draw.line(self.screen, "white", (400, (line * 30)), (400, ((line * 30) + 20)), 6)

                ball.draw(self.screen)
                ball.update()
            else:
                self.screen.fill("black")
                intro_text = self.text_font.render("PONG GAME", False, "white")
                intro_text_rect = intro_text.get_rect(center=(400, 150))
                self.start_button()
                self.level(150, self.button_easy, self.button_easy_rect, self.button_easy_color)
                self.level(400, self.button_medium, self.button_medium_rect, self.button_medium_color)
                self.level(650, self.button_hard, self.button_hard_rect, self.button_hard_color)
                self.screen.blit(intro_text, intro_text_rect)

            pg.display.update()
            self.clock.tick(60)

    def change_level(self):
        if self.button_easy_color == "white":
            return [-5, 5]
        elif self.button_medium_color == "white":
            return [-8, 8]
        elif self.button_hard_color == "white":
            return [-10, 10]

    def button_click_start(self):
        return "darkgrey"

    def button_click_level(self):
        self.button_easy_color = "black"
        self.button_medium_color = "black"
        self.button_hard_color = "black"
        return "white"

    def level(self, x, surf, rect, color):
        self.box.center = (x, 300)
        pg.draw.rect(self.screen, "black", self.box)
        pg.draw.rect(self.screen, color, self.box, 3)
        rect.center = self.box.center
        self.screen.blit(surf, rect)

    def start_button(self):
        self.box.center = (400, 400)
        pg.draw.rect(self.screen, self.button_start_color, self.box)
        pg.draw.rect(self.screen, "white", self.box, 3)
        self.button_start_rect.center = self.box.center
        self.screen.blit(self.button_start, self.button_start_rect)


pong = Game()

ball = pg.sprite.GroupSingle()
ball.add(Ball())

platform = pg.sprite.Group()
platform.add(Platform("p1"))
platform.add(Platform("p2"))

pong.main()
